/*
 * fifo.h
 *
 *  Created on: 12 апр. 2016 г.
 *      Author: hmhamster
 */

#ifndef INC_FIFO_H_
#define INC_FIFO_H_

#include "stdbool.h"
#include "stdint.h"
#include "stddef.h"

typedef struct fifo_t
{
    uint8_t *buffer;     	// data buffer
    uint8_t *buffer_end; 	// end of data buffer
    size_t capacity;  		// maximum number of items in the buffer
    size_t count;     		// number of items in the buffer
    uint8_t *head;       	// pointer to head
    uint8_t *tail;       	// pointer to tail
} fifo_t;

void fifo_init(fifo_t* fifo, uint8_t* buffer, size_t capacity);
bool fifo_push(fifo_t* fifo, uint8_t item);
bool fifo_pop(fifo_t* fifo, uint8_t* item);

#endif /* INC_FIFO_H_ */
