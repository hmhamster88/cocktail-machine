/*
 * hx711.h
 *
 *  Created on: 15 апр. 2016 г.
 *      Author: hmhamster
 */

#ifndef INC_HX711_H_
#define INC_HX711_H_

#include "stdbool.h"
#include "stm32f0xx_hal.h"

typedef struct hx711_t
{
	GPIO_TypeDef* sck_port;
	uint16_t sck_pin;
	GPIO_TypeDef* data_port;
	uint16_t data_pin;

}hx711_t;

bool hx711_data_ready(hx711_t* hx711);
bool hx711_get_value(hx711_t* hx711, unsigned int* value);

#endif /* INC_HX711_H_ */
