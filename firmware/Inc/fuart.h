/*
 * fuart.h
 *
 *  Created on: 12 апр. 2016 г.
 *      Author: hmhamster
 */

#ifndef INC_FUART_H_
#define INC_FUART_H_

#include "fifo.h"
#include "stm32f0xx_hal.h"

typedef struct fuart_t
{
	fifo_t rx_fifo;
	fifo_t tx_fifo;
	UART_HandleTypeDef *huart;
}fuart_t;

void fuart_init(fuart_t* fuart, UART_HandleTypeDef *huart, int rx_buffer_size, int tx_buffer_size);
void fuart_IRQHandler(fuart_t* fuart);
uint8_t fuart_getc(fuart_t* fuart);
bool fuart_getc_nonblock(fuart_t* fuart, uint8_t* c);
void fuart_putc(fuart_t* fuart, uint8_t c);
void fuart_puts(fuart_t* fuart, const char* str);
void fuart_puti(fuart_t* fuart, int value, int base);
void fuart_getsn(fuart_t* fuart, char* str, int count);

#endif /* INC_FUART_H_ */
