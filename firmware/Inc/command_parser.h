/*
 * command_parser.h
 *
 *  Created on: 13 апр. 2016 г.
 *      Author: hmhamster
 */

#ifndef INC_COMMAND_PARSER_H_
#define INC_COMMAND_PARSER_H_

#include "fuart.h"

typedef struct command_parser_t
{
	fuart_t* fuart;
	uint8_t* buffer;
	uint8_t* buffer_end;
	uint8_t* buffer_ptr;
} command_parser_t;

void commnd_parser_init(command_parser_t* parser, fuart_t* fuart, int buffer_size);
void commnd_parser_process_input(command_parser_t* parser);

#endif /* INC_COMMAND_PARSER_H_ */
