/*
 * command_parser.c
 *
 *  Created on: 13 апр. 2016 г.
 *      Author: hmhamster
 */
#include "command_parser.h"
#include "stdbool.h"
#include "stdlib.h"

void commnd_parser_init(command_parser_t* parser, fuart_t* fuart, int buffer_size)
{
	parser->fuart = fuart;
	parser->buffer_ptr =
	parser->buffer = malloc(buffer_size);
	parser->buffer_end = parser->buffer + buffer_size;
}

bool starts_with(const char* string, const char* starts)
{
	while(*string != '\0' && *starts != '\0')
	{
		if(*string != *starts)
		{
			return false;
		}
		string++;
		starts++;
	}
	return *string == *starts;
}

inline bool is_digit(char c)
{
	return c >= '0' && c <= '9';
}

void commnd_parser_process_line(command_parser_t* parser)
{
	const char* line = (const char*)parser->buffer;
	if(starts_with(line, "version"))
	{
		fuart_puti(parser->fuart, 1, 10);
		return;
	}

	fuart_puts(parser->fuart, "unknown command ");
	fuart_puts(parser->fuart, line);
	fuart_puts(parser->fuart, "\r\n");
}

void commnd_parser_process_input(command_parser_t* parser)
{
	if(fuart_getc_nonblock(parser->fuart, parser->buffer_ptr))
	{
		if(*parser->buffer_ptr == '\r' || *parser->buffer_ptr == '\n')
		{
			*parser->buffer_ptr = '\0';
			commnd_parser_process_line(parser);
			parser->buffer_ptr = parser->buffer;
		}
		else
		{
			parser->buffer_ptr++;
			if(parser->buffer_ptr == parser->buffer_end)
			{
				parser->buffer_ptr = parser->buffer;
			}
		}
	}
}

