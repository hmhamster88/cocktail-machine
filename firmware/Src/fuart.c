/*
 * fuart.c
 *
 *  Created on: 12 апр. 2016 г.
 *      Author: hmhamster
 */
#include "fuart.h"
#include "stdlib.h"


void fuart_init(fuart_t* fuart, UART_HandleTypeDef *huart, int rx_buffer_size, int tx_buffer_size)
{
	fuart->huart = huart;
	uint8_t* rx_buffer = (uint8_t*)malloc(rx_buffer_size);
	uint8_t* tx_buffer = (uint8_t*)malloc(tx_buffer_size);

	fifo_init(&fuart->rx_fifo, rx_buffer, rx_buffer_size);
	fifo_init(&fuart->tx_fifo, tx_buffer, tx_buffer_size);

	__HAL_UART_ENABLE_IT(huart, UART_IT_RXNE);
}

void fuart_IRQHandler(fuart_t* fuart)
{
	UART_HandleTypeDef *huart = fuart->huart;
	if ((__HAL_UART_GET_IT(huart, UART_IT_RXNE) != RESET)
			&& (__HAL_UART_GET_IT_SOURCE(huart, UART_IT_RXNE) != RESET))
	{
		uint8_t data = huart->Instance->RDR;
		fifo_push(&fuart->rx_fifo, data);
		__HAL_UART_SEND_REQ(huart, UART_RXDATA_FLUSH_REQUEST);
	}
	if ((__HAL_UART_GET_IT(huart, UART_IT_TXE) != RESET)
			&& (__HAL_UART_GET_IT_SOURCE(huart, UART_IT_TXE) != RESET))
	{
		uint8_t data;
		if (fifo_pop(&fuart->tx_fifo, &data))
		{
			huart->Instance->TDR = data;
		}
		else
		{
			__HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
			__HAL_UART_ENABLE_IT(huart, UART_IT_TC);
		}
	}
}

uint8_t fuart_getc(fuart_t* fuart)
{
	UART_HandleTypeDef *huart = fuart->huart;
	__HAL_UART_ENABLE_IT(huart, UART_IT_RXNE);
	uint8_t c;
	while(!fifo_pop(&fuart->rx_fifo, &c));
	return c;
}

bool fuart_getc_nonblock(fuart_t* fuart, uint8_t* c)
{
	return fifo_pop(&fuart->rx_fifo, c);
}

void fuart_putc(fuart_t* fuart, uint8_t c)
{
	UART_HandleTypeDef *huart = fuart->huart;
	__HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
	fifo_push(&fuart->tx_fifo, c);
	__HAL_UART_ENABLE_IT(huart, UART_IT_TXE);
}

void fuart_puts(fuart_t* fuart, const char* str)
{
	while(*str != 0)
	{
		fuart_putc(fuart, (uint8_t)*str);
		str++;
	}
}

#define MAX_NUM_SIZE 12 //for 32 signed integer

void fuart_puti(fuart_t* fuart, int value, int base)
{
	char buffer[MAX_NUM_SIZE];
	itoa(value, buffer, base);
	fuart_puts(fuart, buffer);
}

void fuart_getsn(fuart_t* fuart, char* str, int count)
{
	uint8_t c;
	count--;
	while(1)
	{
		c = fuart_getc(fuart);
		*str = c;
		str++;
		count--;
		if(count == 0)
		{
			*str = '\0';
			return;
		}
		if(c == '\n' || c == '\r')
		{
			*str = '\0';
			return;
		}
	}
}
