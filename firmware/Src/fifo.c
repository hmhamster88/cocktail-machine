/*
 * fifo.c
 *
 *  Created on: 12 апр. 2016 г.
 *      Author: hmhamster
 */
#include "fifo.h"

void fifo_init(fifo_t* fifo, uint8_t* buffer, size_t capacity)
{
	fifo->buffer = buffer;
	fifo->buffer_end = fifo->buffer + capacity * sizeof(uint8_t);
	fifo->capacity = capacity;
	fifo->count = 0;
	fifo->head = buffer;
	fifo->tail = buffer;
}

bool fifo_push(fifo_t* fifo, uint8_t item)
{
	if(fifo->count == fifo->capacity)
	{
		return false;
	}
	*fifo->head = item;
	fifo->head = fifo->head + sizeof(uint8_t);
	if(fifo->head == fifo->buffer_end)
	{
		fifo->head = fifo->buffer;
	}
	fifo->count++;
	return true;
}

bool fifo_pop(fifo_t* fifo, uint8_t* item)
{
	if(fifo->count == 0)
	{
		return false;
	}
	*item = *fifo->tail;
	fifo->tail = fifo->tail + sizeof(uint8_t);
	if(fifo->tail == fifo->buffer_end)
	{
		fifo->tail = fifo->buffer;
	}
	fifo->count--;
	return true;
}
