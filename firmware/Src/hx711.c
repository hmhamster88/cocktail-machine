/*
 * hx711.c
 *
 *  Created on: 15 апр. 2016 г.
 *      Author: hmhamster
 */

#include "hx711.h"

inline bool data_pin(hx711_t* hx711)
{
	return HAL_GPIO_ReadPin(hx711->data_port, hx711->data_pin) == GPIO_PIN_SET;
}

inline void set_clk_pin(hx711_t* hx711, bool set)
{
	HAL_GPIO_WritePin(hx711->sck_port, hx711->sck_pin, set?GPIO_PIN_SET: GPIO_PIN_RESET);
}

bool hx711_data_ready(hx711_t* hx711)
{
	return !data_pin(hx711);
}

bool hx711_get_value(hx711_t* hx711, unsigned int* value)
{
	if(!hx711_data_ready(hx711))
	{
		return false;
	}
	unsigned int adc_value = 0;
	for(int i = 0; i < 24; i++)
	{
		set_clk_pin(hx711,true);
		adc_value <<= 1;
		set_clk_pin(hx711,false);
		if(data_pin(hx711))
		{
			adc_value++;
		}
	}
	*value = adc_value;
	set_clk_pin(hx711,true);
	set_clk_pin(hx711,false);
	return true;
}

