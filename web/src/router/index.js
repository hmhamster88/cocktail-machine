import Vue from 'vue'
import Router from 'vue-router'
import VueResource from 'vue-resource'

import Cocktails from '@/components/Cocktails'
import Cocktail from '@/components/Cocktail'
import Ingredients from '@/components/Ingredients'
import Ingredient from '@/components/Ingredient'

Vue.use(Router)
Vue.use(VueResource)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'cocktails',
      component: Cocktails
    },
    {
      path: '/cocktail/:id',
      name: 'cocktail',
      component: Cocktail
    },
    {
      path: '/ingredients/all',
      name: 'ingredients',
      component: Ingredients
    },
    {
      path: '/ingredients/:id',
      name: 'ingredient',
      component: Ingredient
    }
  ]
})
