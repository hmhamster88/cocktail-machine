var express = require('express');
var backend = require('./backend');

var app = express();
app.use('/', express.static('./dist'));
backend(app);
app.listen(8081, function () {
  console.log('Example app listening on port 81!');
});
