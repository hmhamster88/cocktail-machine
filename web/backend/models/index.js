/**
 * Created by hmham on 3/12/2017.
 */

var Datastore = require('nedb');
var fs = require('fs');
var path = require('path')
var Enumerable = require('linq');

var dbPath = '../../database';

function collectionPath(name) {
  return path.join(__dirname, dbPath, name);
}

var ingredientsPath = collectionPath('ingredients.json');


var db = {
  ingredients: new Datastore(ingredientsPath),
  cocktails: new Datastore(collectionPath('cocktails.json'))
};

db.ingredients.loadDatabase();
db.cocktails.loadDatabase();

db.ingredients.new = function (callback) {
  this.insert({
    name: 'Новый ингредиент',
    description: '',
    image: ''
  }, callback);
};

db.cocktails.new = function (callback) {
  this.insert({
    name: 'Новый коктель',
    description: '',
    image: '',
    steps: []
  }, callback);
};

db.cocktails.idsToObjects = function (cocktails, callback) {
  var ids = Enumerable.from(cocktails)
    .selectMany(c => Enumerable.from(c.steps).select(s => s.ingredient)).distinct().toArray();

  db.ingredients.find({_id: {$in: ids}}, function (err, ingredients) {
    var map = {};
    ingredients.forEach(i => map[i._id] = i);
    cocktails.forEach(cocktail => {
      cocktail.steps.forEach(step => {
        step.ingredient = map[step.ingredient];
      });
    });
    callback();
  });
};

db.cocktails.objectsToIds = function (cocktails, callback) {
  cocktails.forEach(cocktail => {
    cocktail.steps.forEach(step => {
      step.ingredient = step.ingredient._id;
    });
  });
  callback();
};

module.exports = db;
