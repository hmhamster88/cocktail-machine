/**
 * Created by hmham on 3/12/2017.
 */

var express = require('express');
var bodyParser = require('body-parser');
var models = require('./../models/index');
var ImageService = require('../services/image-service')

var router = express.Router();
router.use(bodyParser.json())

function createRoutesForModel(modelName) {
  var model = models[modelName];

  router.get('/' + modelName + '/all', function (req, res) {
    model.find({},function (err, docs) {
      if(model.idsToObjects) {
        model.idsToObjects(docs, function () {
          res.json(docs);
        });
      } else {
        res.json(docs);
      }
    });
  });

  router.get('/' + modelName + '/:id', function (req, res) {
    model.findOne({_id: req.params.id}, function (err, doc) {
      if(model.idsToObjects) {
        model.idsToObjects([doc], function () {
          res.json(doc);
        });
      } else {
        res.json(doc);
      }
    });
  });

  router.post('/' + modelName, function (req, res) {
    model.new(function (err, docs) {
      res.json(docs);
    });
  });

  router.delete('/' + modelName + '/:id', function (req, res) {
    model.remove({_id: req.params.id}, function (err, numRemoved) {
      res.json(numRemoved);
    });
  });

  router.put('/' + modelName, function (req, res) {
    var toUpdate = req.body;
    if(model.objectsToIds) {
      model.objectsToIds([toUpdate], () => {
        model.update({_id: toUpdate._id}, toUpdate, (err, numReplaced) => {
          res.json(toUpdate);
        });
      });
    } else {
      model.update({_id: toUpdate._id}, toUpdate, (err, numReplaced) => {
        res.json(toUpdate);
      });
    }
  });
}

createRoutesForModel('ingredients');
createRoutesForModel('cocktails');
var imageService = new ImageService(router);

module.exports = router;
