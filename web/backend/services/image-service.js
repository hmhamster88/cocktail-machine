/**
 * Created by hmham on 3/19/2017.
 */
var path = require('path');
var express = require('express');
var shortid = require('short-id');

function imageService(router) {
  var imagesDir = path.resolve(__dirname,'../../images');
  router.use('/images', express.static(imagesDir));
  router.post('/images/upload', function(req, res) {
    if (!req.files) {
      return res.status(400).send('No files were uploaded.');
    }

    let sampleFile = req.files.file;
    var ext = path.extname(sampleFile.name);
    var newFileName = shortid.generate() + ext;
    var newPath = path.resolve(imagesDir, newFileName);
    sampleFile.mv(newPath, function(err) {
      if (err) {
        console.log(err);
        return res.status(500).send(err);
      }

      res.send(newFileName);
    });
  });
}

module.exports = imageService;
