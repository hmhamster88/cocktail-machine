/**
 * Created by hmham on 3/18/2017.
 */
var fileUpload = require('express-fileupload');
var apiRouter = require('./router');

function backend(app) {
  app.use(fileUpload());
  app.use('/api', apiRouter);
}

module.exports = backend;
